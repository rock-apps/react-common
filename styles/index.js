export const FontSize = {
    xmini: 8,
    mini: 10,
    xxsmall: 12,
    xsmall: 14,
    small: 16,
    medium: 18,
    large: 20,
    xlarge: 22,
    xxlarge: 24,
    title: 32,
};

const baseColor = {
    primary: '#FFA063',
    secondary: '#FF7F69',
    primaryText: 'white',
    secondaryText: '#333333',
    description: '#666666',
    primaryButtonBackground: 'white',
    primaryBackgroundColor: 'white',
    secondaryBackgroundColor: '#E5E5E5',
    underlayColor: '#EFEFEF',
    error: '#EB5757',
};

export const Color = {
    ...baseColor,
    gradient: [baseColor.primary, baseColor.secondary],
};
