import React from 'react';
import { ActivityIndicator } from 'react-native';

import ContainerView from '../ContainerView';

export default (props) => {
    const {
        size = 'large',
        color = '#ffffff',
        backgroundColor = 'rgba(0, 0, 0, 0.1)',
    } = props;

    return (
        <ContainerView style={{ backgroundColor }}>
            <ActivityIndicator size={size} color={color} />
        </ContainerView>
    );
};
