import React, { Component } from 'react';
import StringMask from 'string-mask';
import styled, { css } from 'styled-components/native';
import iMask from 'imask';

import Icon from '../Icons';
import { Color, FontSize } from '../../styles';

const inputPadding = 20;
const inputTextHeight = 17;
const inputHeight = (inputPadding * 2) + inputTextHeight;

const buttonSize = 30;
const buttonOffset = (inputHeight - buttonSize) / 2;

const WrapperView = styled.View`
    margin: 0;
    margin-top: 15px;
`;

const InputContainer = styled.View`
    border-radius: ${inputPadding * 2}px;
    background-color: ${props => props.backgroundColor};
    overflow: hidden;
    ${props => props.error && css`
        background-color: rgba(255, 0, 0, 0.1);
    `}
`;

const Input = styled.TextInput`
    padding: ${inputPadding}px;
    background-color: transparent;
    color: ${props => props.textColor};
    border: 0;
    border-color: ${Color.error};
    font-weight: bold;
    width: 100%;
    max-width: 100%;
    overflow: hidden;
`;

const CallToActionButton = styled.TouchableOpacity`
    background-color: ${Color.primaryText};
    width: ${buttonSize}px;
    height: ${buttonSize}px;
    border-radius: ${buttonSize / 2}px;
    position: absolute;
    top: ${buttonOffset}px;
    right: ${buttonOffset}px;
    display: flex;
    justify-content: center;
    align-items: center;
`;

const TextCounter = styled.Text`
    color: ${Color.primaryText};
    font-size: ${FontSize.xxsmall};
    text-align: right;
    font-weight: bold;
    margin-right: 15px;
    margin-top: 5px; 
`;

const ErrorText = styled.Text`
    color: ${Color.error};
    font-size: ${FontSize.xxsmall};
    margin-top: 5px;
    margin-left: 10px;
    margin-right: 10px;
`;

class StyledTextInput extends Component {
    constructor() {
        super();

        this.input = null;
    }

    focus = () => {
        this.input.root.focus();
    }

    getBackgroundColor = () => {
        const { backgroundColor, disabled } = this.props;
        return backgroundColor || `rgba(255, 255, 255, ${disabled ? 0.15 : 0.3})`;
    }

    getPlaceholderTextColor = () => {
        const { placeholderTextColor, disabled } = this.props;
        return placeholderTextColor || `rgba(255, 255, 255, ${disabled ? 0.5 : 0.8})`;
    }

    getTextColor = () => {
        const { textColor, disabled } = this.props;
        return textColor || `rgba(255, 255, 255, ${disabled ? 0.75 : 1})`;
    }

    applyMask = (value) => {
        if (value && this.props.mask) {
            console.log('Default Mask')

            const formatter = new StringMask(this.props.mask);
            const cleanValue = value.replace(/[-'`~!@#$%^&*()_|+=?;:'",.<>\{\}\[\]\\\/]/gi, '');
            const newValue = formatter.apply(cleanValue);
            return newValue;
        }

        return value;
    }
    applyMaskNumber = (value) => {
        if (value && this.props.iMask) {
            console.log('iMask')

            var masked = IMask.createMask({
                mask: '[####],{##}',
                definitions: {
                    '#': /[0-9]/
                }
                // ...and other options
            });
            var maskedValue = masked.resolve(value);

// mask keeps state after resolving
            return masked.value;  // same as maskedValue
// get unmasked value
        }

        return value;
    }

    render() {
        const myProps = Object.assign(this.props, {});

        const {
            error,
            disabled = false,
            counterStyle,
            inputStyle,
            maxLength = 320,
            showCounter,
            style,
            value,
            callToAction,
            maskNumber,
        } = myProps;

        var formatt;
        if(maskNumber) {
            formatt = this.applyMaskNumber(value);
        }else {
            formatt = this.applyMask(value);
        }

        const formattedValue = formatt

        // Style properly has to be applied only to the top level element and
        // backgrounColor has to be applied only to the input container,
        // that's why we need to remove them from props.
        delete myProps.backgroundColor;
        delete myProps.style;

        const textInputStyle = { ...inputStyle };
        if (callToAction) {
            textInputStyle.paddingRight = 60;
        }

        return (
            <WrapperView style={ style }>
                <InputContainer
                    backgroundColor={ this.getBackgroundColor() }
                    error={ myProps.error }
                >
                    <Input
                        { ...myProps }
                        value={ formattedValue }
                        ref={ (input) => { this.input = input; } }
                        textColor={ this.getTextColor() }
                        placeholderTextColor={ this.getPlaceholderTextColor() }
                        underlineColorAndroid='transparent'
                        editable={ !disabled }
                        selectTextOnFocus={ !disabled }
                        style={ textInputStyle }
                    />
                    { callToAction &&
                    <CallToActionButton
                        onPress={ callToAction }
                    >
                        <Icon
                            name="iconSearch"
                            width="20"
                            height="20"
                            viewBox="-0.5 0 20 20"
                            stroke="transparent"
                            fill="#FF7F69"
                        />
                    </CallToActionButton>
                    }
                </InputContainer>
                { showCounter &&
                <TextCounter
                    style={ counterStyle }
                >
                    { value ? value.length : 0 }/{ maxLength }
                </TextCounter>
                }
                { !!error &&
                <ErrorText>{ error }</ErrorText>
                }
            </WrapperView>
        );
    }
}

export default StyledTextInput;
