import React from 'react';

import LinearGradient from 'react-native-linear-gradient';

import { ifIphoneX } from 'react-native-iphone-x-helper';

import { Color } from '../../styles';

export default props => (
    <LinearGradient
        colors={ Color.gradient }
        start={{ x: 0.0, y: 0.2 }}
        end={{ x: 0.6, y: 1.0 }}
        style={{
            flexDirection: 'row',
            alignItems: 'flex-end',
            backgroundColor: 'transparent',
            ...ifIphoneX({
                height: 95,
            }, {
                height: 65,
            }),
            width: '100%',
        }}
    >
        { props.children }
    </LinearGradient>
);
