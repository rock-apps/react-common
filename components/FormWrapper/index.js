import React from 'react';

import LinearGradient from 'react-native-linear-gradient';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import styled from 'styled-components/native';

import LoaderView from '../LoaderView';
import CloseButton from '../Buttons/CloseButton';

import { Color } from '../../styles';

const Wrapper = styled.View`
    display: flex;
    flexDirection: column;
    flex: 1;
    padding: 20px;
    paddingTop: 80px;
    height: 100%;
`;

export default props => (
    <LinearGradient
        colors={ Color.gradient }
        start={{ x: 0.0, y: 0.2 }}
        end={{ x: 0.6, y: 1.0 }}
        style={{ backgroundColor: 'transparent', display: 'flex', flex: 1 }}
    >
        {
            props.modalStyle &&
            props.onPressCloseButton &&
            <CloseButton onPress={ props.onPressCloseButton }/>
        }
        <KeyboardAwareScrollView
            resetScrollToCoords={{ x: 0, y: 0 }}
            style={{ width: '100%', height: '100%' }}
            scrollEnabled={ props.isScrollEnabled === null ? true : props.isScrollEnabled }
        >
            <Wrapper {...props}>
                {props.children}
            </Wrapper>
        </KeyboardAwareScrollView>
        {props.isLoading && <LoaderView />}
    </LinearGradient>
);
