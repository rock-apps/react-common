import React from 'react';

import styled from 'styled-components/native';

import { FontSize } from '../../styles';

const Header = styled.Text`
    color: ${props => (props.color ? props.color : '#fff')};
    margin: ${props => (props.margin ? props.margin : 0)};
    font-size: ${props => props.fontSize};
    line-height: ${props => props.fontSize + 6};
    text-align: ${props => (props.align ? props.align : 'center')}
    font-weight: bold;
`;

export const H1 = props => (
    <Header
        color={props.color}
        margin={props.margin}
        fontSize={FontSize.title}
        align={props.align}
    >
        {props.children}
    </Header>
);

export const H2 = props => (
    <Header
        color={props.color}
        margin={props.margin}
        fontSize={FontSize.xxlarge}
        align={props.align}
    >
        {props.children}
    </Header>
);

export const H3 = props => (
    <Header
        color={props.color}
        margin={props.margin}
        fontSize={FontSize.medium}
        align={props.align}
    >
        {props.children}
    </Header>
);
