import React from 'react';
import SvgIcon from 'react-native-svg-icon';
import svgs from './svgs';

export default props => (
    <SvgIcon {...props} svgs={svgs} />
);
