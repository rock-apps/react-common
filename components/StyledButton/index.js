import React from 'react';

import styled from 'styled-components/native';

import { Color, FontSize } from '../../styles';

const ActionButton = styled.TouchableHighlight`
    border-radius: 40;
    display: flex;
    margin: 0;
    padding: 20px;
    background-color: rgba(255, 255, 255, 1);
    border: 0;
    margin-top: ${props => props.marginTop || '40px'};
    align-items: center;
`;

const ButtonText = styled.Text`
    color: ${Color.secondary};
    font-size: ${FontSize.large};
    font-weight: bold;
`;

export default props => (
    <ActionButton {...props}>
        <ButtonText>
            { props.title }
        </ButtonText>
    </ActionButton>
);
