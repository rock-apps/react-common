import React from 'react';

import styled from 'styled-components/native';

import * as Assets from '../../assets';

import { Color } from '../../styles';

const Avatar = styled.Image`
    background-color: transparent;
    width: ${props => `${props.size}px`};
    height: ${props => `${props.size}px`};
    border-radius: ${props => `${props.size / 2}px`};
    overflow: hidden;
`;

export default (props) => {
    const {
        size = 50,
        source = Assets.Image.avatarPlaceholder,
    } = props;

    return (
        <Avatar
            key= { source.uri || 'placeholder' }
            source={ source }
            resizeMode='cover'
            size={ size }
        />
    );
};
