import React from 'react';

import styled from 'styled-components/native';

import * as Assets from '../../assets';

const Logo = styled.Image`
    alignSelf: center;
`;

export default props => (
    <Logo
        {...props}
        source={ Assets.Image.whiteLogo }
        resizeMode='contain'
    />
);
