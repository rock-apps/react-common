import React from 'react';

import {
    View,
    TouchableHighlight,
    Text,
} from 'react-native';

import styled from 'styled-components/native';
import Icon from '../Icons';

const CounterWrapper = styled.View`
    border: 1px solid rgba(0, 0, 0, 0.24);
    flex-direction: row;
    width: 120px;
    height: 40px;
    border-radius: 20px;
    overflow: hidden;
`;

const Button = styled.TouchableHighlight`
    flex: 1;
    align-items: center;
    justify-content: center;
`;

const Amount = styled.Text`
    flex: 1;
    text-align: center;
    font-size: 16px;
    line-height: 36px;
    font-weight: bold;
    color: #333333;
`;

export default function Counter(props) {
    return (
        <CounterWrapper>
            <Button
                onPress={ props.handleDecrement }
                underlayColor="rgba(0, 0, 0, 0.24)"
            >
                <Icon
                    name="iconMinus"
                    width="15"
                    height="15"
                    viewBox="-6 0 20 20"
                    stroke="transparent"
                    fill="#808080"
                />
            </Button>
            
            <Amount>
                { props.amount }
            </Amount>

            <Button
                onPress={ props.handleIncrement }
                underlayColor="rgba(0, 0, 0, 0.24)"
            >
                <Icon
                    name="iconPlus"
                    width="15"
                    height="15"
                    viewBox="5 3 20 20"
                    stroke="transparent"
                    fill="#808080"
                />
            </Button>
        </CounterWrapper>
    );
}
