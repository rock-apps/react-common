
import styled from 'styled-components/native';

export default styled.View`
    position: absolute;
    bottom: 0;
    right: ${props => (props.right ? props.right : 0)};
    left: ${props => (props.left ? props.left : 0)};
    height: 1;
    backgroundColor: ${props => (props.backgroundColor ? props.backgroundColor : 'rgba(0, 0, 0, 0.08)')};
`;
