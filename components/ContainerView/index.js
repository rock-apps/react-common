import styled from 'styled-components/native';

export default styled.View`
    position: absolute;
    left: 0;
    right: 0;
    top: 0;
    bottom: 0;
    width: 100%;
    height: 100%;
    alignItems: center;
    justifyContent: center;
    background-color: transparent;
`;
