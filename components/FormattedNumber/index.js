import React from 'react';
import numeral from 'numeral';

import { Text } from 'react-native';

numeral.register('locale', 'pt-br', {
    delimiters: {
        thousands: '.',
        decimal: ','
    },
    abbreviations: {
        thousand: 'mil',
        million: 'milhões',
        billion: 'b',
        trillion: 't'
    },
    ordinal: function(number) {
        return 'º';
    },
    currency: {
        symbol: 'R$'
    }
});

numeral.locale('pt-br');

const FormattedNumber = ({ value, format = '$0,0.00', style }) => (
    <Text style={{ ...style }}>
        { numeral(Number(value)).format(format) }
    </Text>
);

export default FormattedNumber;
