import React from 'react';

import StyledTextInput from '../StyledTextInput';

export default props => (
    <StyledTextInput
        ref={ (input) => { this.input = input; } }
        backgroundColor={ props.backgroundColor }
        textColor={ props.textColor }
        placeholderTextColor={ props.placeholderTextColor || "#fff" }
        style={ props.style }
        inputStyle={{
            height: 120,
            ...props.inputStyle,
        }}
        maxLength={ props.maxLength }
        numberOfLines={ props.numberOfLines }
        placeholder={ props.placeholder }
        onChangeText={ props.onChangeText }
        onSubmitEditing={ props.onSubmitEditing }
        value={ props.value }
        error={ props.error }
        autoCapitalize={ props.autoCapitalize }
        autoCorrect={ props.autoCorrect }
        enablesReturnKeyAutomatically={ props.enablesReturnKeyAutomatically }
        returnKeyType={ props.returnKeyType }
        disabled={ props.disabled }
        showCounter={ props.showCounter }
        counterStyle={ props.counterStyle }
        multiline
    />
);
