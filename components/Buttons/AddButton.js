import React from 'react';

import styled from 'styled-components/native';

import { Color } from '../../styles';
import Icon from '../Icons';

const AddButton = styled.TouchableOpacity`
    border-radius: 100;
    display: flex;
    margin: 0;
    padding: 15px;
    background-color: ${Color.secondary};
    border: 0;
    margin-top: 20px;
    align-items: center;
`;

export default props => (
    <AddButton onPress={props.onPress}>
        <Icon
            name="iconPlus"
            width="50"
            height="50"
            viewBox="-0.5 0 25 25"
            stroke="transparent"
            fill="#fff"
        />
    </AddButton>
);
