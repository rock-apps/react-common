import React from 'react';

import styled, { css } from 'styled-components/native';

import Icon from '../Icons';

const CloseButton = styled.TouchableOpacity`
    background-color: transparent;
    align-items: center;
    width: 32px;
    height: 32px;
    z-index: 100;
    ${props => !props.isNavButton && css`
        position: absolute;
        top: 40px;
        left: 12px;
    `}
`;

export default props => (
    <CloseButton { ...props }>
        <Icon
            name="iconClose"
            width="32"
            height="32"
            viewBox="0 0 25 25"
            stroke="#fff"
            fill="#fff"
        />
    </CloseButton>
);
